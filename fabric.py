#!/usr/bin/env python
# -*- coding: utf-8 -*-

import re
import pygame
import sys
import random
import argparse
import timeit

from numpy import std, mean
from os import system
from math import sqrt
from copy import deepcopy
from pygame.locals import *

t = 5
f = 5

class Fabric(object):
	def __init__(self, i, j):
		self.i = i
		self.j = j
		self.status = False
	
	def is_fixed(self):
		return self.status
	
	def fix(self):
		self.status = True

class Robot(object):
	def __init__(self, i, j, radius, env, fabrics):
		#Env is the environment and stuff
		self.env = env
		self.i = i
		self.j = j
		
		self.r = radius

		#The env has 5 tools
		self.tools = [0 for i in range (t)]
		#Stores the position of each fabric
		self.fabrics = fabrics

		#Needs at most 2 of each tool
		self.needed = [-2 for i in range(t)]

		self.cost = 0

	def fixed_all(self):
		for fabric in self.fabrics:
			if not fabric.is_fixed():
				return False
		return True

	def distance(self, i, j):
		return max([abs(self.i-i), abs(self.j-j)])
		#return sqrt((self.i-i)**2 + (self.j+j)**2)

	#The tool idx corresponds to the fabric it can fix
	def can_fix(self):
		can_fix = []
		if self.tools[0] >= 2 and not self.fabrics[0].is_fixed(): #Fixing melhoramento genético
			fab = self.fabrics[0]
			can_fix.append((0, self.distance(fab.i, fab.j)))
		if self.tools[1] >= 2 and not self.fabrics[1].is_fixed(): #Fixing manutenção de cascos
			fab = self.fabrics[1]
			can_fix.append((1, self.distance(fab.i, fab.j)))
		if self.tools[2] >= 2 and not self.fabrics[2].is_fixed(): #Fixing petrolífera
			fab = self.fabrics[2]
			can_fix.append((2, self.distance(fab.i, fab.j)))
		if self.tools[3] >= 2 and not self.fabrics[3].is_fixed(): #Fixing superaquecimento caldeiras
			fab = self.fabrics[3]
			can_fix.append((3, self.distance(fab.i, fab.j)))
		if self.tools[4] >= 2 and not self.fabrics[4].is_fixed(): #Fixing indústria de vidas de aço
			fab = self.fabrics[4]
			can_fix.append((4, self.distance(fab.i, fab.j)))
		
		#Can't fix anything
		if len(can_fix) == 0:
			return -1
		else:
			closest = min(can_fix, key=lambda e: e[1])
			#Index 0 is the fabric id
			return closest[0]

	def has_all_itens(self):
		for tool in self.needed:
			if tool < 0:
				return False
		return True

	#Returns if it needs an tool or not
	def need_tool(self, tool):
		return self.needed[tool] < 0
	
	#Moves to fabricID using the A* algorithm. Collects tools while walking there
	def fix_fabric(self, fab_id):
		fab = self.fabrics[fab_id]		
		#Where I am
		root = self.env.nodes[self.i][self.j]
		#Where I want to go
		dest = self.env.nodes[fab.i][fab.j]
		
		future = self.cherbyshev_distance(dest)
	
		nodes = [root]
		visited = []
		while True:
			#Expand the lowest cost
			lowest = min(nodes, key=lambda n: n.acc)
			visited.append(lowest)
			
			lowest.expand()

			if lowest == dest:
				#Found fabric: now we need to fix it
				self.fabrics[fab_id].fix()
				self.env.nodes[fab.i][fab.j].fixed = True

				self.tools[fab_id] -= 2
				#Updates current position
				self.i = fab.i
				self.j = fab.j

				#print('Fixing fabric %d' % fab_id)
				#How much the path cost
				self.cost += lowest.backtrack(root)
				return lowest, len(visited)
			
			#Remove from nodes list and append all its adjacencies
			nodes.remove(lowest)
			for node in lowest.adj():
				if not node in nodes and not node in visited:
					node.father = lowest
					node.acc = lowest.acc + node.cost + future[node.i][node.j]
					nodes.append(node)
		self.env.reprint()

	def cherbyshev_distance(self, dest):
		ei = dest.i
		ej = dest.j

		distances = [[0 for i in range(42)] for j in range(42)]
		for i in range(42):
			for j in range(42):
				distances[i][j] = max([abs(i-ei), abs(j-ej)])
		return distances

	#Select a random position and move to it instead of randoming moving around its position
	#The robot collects all the tools he finds in the path
	def move(self):
		#Random destination
		di = random.randrange(0, 42)
		dj = random.randrange(0, 42)

		while self.i != di or self.j != dj:
			pos = self.env.nodes[self.i][self.j]
			self.cost += pos.cost
			pos.expand()

			#Found enough tools to fix anything if it's non ordened
			if ordem == 'nordenado':
				fab_id = self.can_fix()
				if fab_id != -1:
					self.fix_fabric(fab_id)
					self.env.reprint()
					return

			#Search items by the closest
			for r in range(self.r):
				neighb = [[(self.i+i)%42, (self.j+j)%42] for i in range(-r,r) for j in range(-r,r) if self.i+i >= 0 and self.j+j >= 0 and self.i+i <= 41 and self.j+j <= 41]
				for pos in neighb:
					node = self.env.nodes[pos[0]][pos[1]]
					tool = node.tool
					if tool != -1:# and self.need_tool(tool):
						if qtf == 'all':
							self.collect_tool(pos[0], pos[1])
							self.env.reprint()
							return
						#Get it if need it
						elif qtf == 'enough':
							if self.need_tool(tool):
								self.collect_tool(pos[0], pos[1])
								self.env.reprint()
								return
			
			if self.i != di:
				if self.i < di:
					self.i += 1
				else:
					self.i -= 1
			if self.j != dj:
				if self.j < dj:
					self.j += 1
				else:
					self.j -= 1		
		self.env.reprint()

	#Uses A* to reach the tool
	def collect_tool(self, i, j):
		root = self.env.nodes[self.i][self.j]
		dest = self.env.nodes[i][j]
		
		future = self.cherbyshev_distance(dest)
		
		nodes = [root]
		visited = []
		while True:
			#Expand the lowest cost
			lowest = min(nodes, key=lambda n: n.acc)
			visited.append(lowest)
			
			lowest.expand()

			if lowest == dest:
				tool = lowest.tool
				if tool != -1:
					self.tools[tool] += 1
					self.needed[tool] += 1
					lowest.tool = -1
					lowest.set_color()
				#How much the path cost
				self.cost += lowest.backtrack(root)

				#Updates robot position
				self.i = i
				self.j = j
				return lowest, len(visited)
			
			#Remove from nodes list and append all its adjacencies
			nodes.remove(lowest)
			for node in lowest.adj():
				if not node in nodes and not node in visited:
					node.father = lowest
					node.acc = lowest.acc + node.cost + future[node.i][node.j]
					nodes.append(node)
		self.env.reprint()

	def print_tools(self):
		print(self.tools)

class Graph(object):
	class Node(object):
		def __init__(self, cost, i, j):
			cost = int(cost)
			if cost == 0:
				    self.cost = 1
			elif cost == 1:
				    self.cost = 5
			elif cost == 2:
				    self.cost = 10
			elif cost == 3:
				    self.cost = 15

			#Used in uniform cost
			self.acc = 0
			self.father = None
			self.tool = -1
			self.fabric = -1
			self.fixed = False

			self.i = i
			self.j = j

			self.north = -1
			self.south = -1
			self.east = -1
			self.west = -1

			self.set_color()
		
			#def __cmp__(self, other):
			#	return 

		def adj(self):
			ways = []
			if self.north != -1:
				ways.append(self.north)
			if self.east != -1:
				ways.append(self.east)
			if self.south != -1:
				ways.append(self.south)
			if self.west != -1:
				ways.append(self.west)
			return ways
	
		def set_color(self):
			p = res/42

			if self.tool != -1:
				self.color = (0, 0, 255)
			elif self.fabric != -1 and self.fixed:
				self.color = (0, 255, 0)
			elif self.fabric != -1 and not self.fixed:
				self.color = (255, 0, 0)
			elif self.cost == 1:
				self.color = (36, 51, 20)
			elif self.cost == 5:
				self.color = (74, 69, 42)
			elif self.cost == 10:
				self.color = (28, 48, 71)
			elif self.cost == 15:
				self.color = (87, 41, 4)
			pygame.draw.rect(screen, self.color, (self.j*p, self.i*p, p, p), sc_w)
	
		def expand(self):
			p = res/42
			
			if self.tool != -1:
				self.color = (0, 0, 255)
			elif self.fabric != -1 and self.fixed:
				self.color = (0, 255, 0)
			elif self.fabric != -1 and not self.fixed:
				self.color = (255, 0, 0)
			elif self.cost == 1:
				self.color = (146, 208, 80)
			elif self.cost == 5:
				self.color = (148, 138, 84)
			elif self.cost == 10:
				self.color = (83, 139, 208)
			elif self.cost == 15:
				self.color = (227, 108, 10)
			pygame.draw.rect(screen, self.color, (self.j*p, self.i*p, p, p), sc_w)
			pygame.display.flip()

		def backtrack(self, initial_node):
			father = self
			cost = 0
			p = res/42
			while father != initial_node:
				father.expand()
				#pygame.draw.rect(screen, (255, 0, 0), (father.j*p, father.i*p, p, p), sc_w)
				#pygame.display.flip()
				cost += father.cost
				father = father.father
			father.expand()
			#pygame.draw.rect(screen, (255, 0, 0), (father.j*p, father.i*p, p, p), sc_w)
			#pygame.display.flip()
			cost += father.cost
			return cost
	
	def __init__(self):
		self.nodes = [[0 for i in range(42)] for j in range(42)]
				
	def build_graph(self, filename):
		f = open(filename, 'r')
		matrix = []
		for row in f:
			matrix.append(re.findall(r'\d+', row))

		for i in range(42):
			for j in range(42):
				self.nodes[i][j] = self.Node(matrix[i][j], i, j)

		for i in range(42):
			for j in range(42):
				if i-1 >= 0:
					self.nodes[i][j].north = self.nodes[i-1][j]
				if i+1 < 42:
					self.nodes[i][j].south = self.nodes[i+1][j]
				if j-1 >= 0:
					self.nodes[i][j].west = self.nodes[i][j-1]
				if j+1 < 42:
					self.nodes[i][j].east = self.nodes[i][j+1]
	
	#Set tools position
	def set_tools(self):
		tools_qtd = [10, 8, 6, 4, 2]

		tools_pos = []
		for i in range(len(tools_qtd)):
			for k in range(tools_qtd[i]):
				rx = random.randrange(0, 42)
				ry = random.randrange(0, 42)
				while self.nodes[rx][ry].tool != -1 or self.nodes[rx][ry].fabric != -1:
					rx = random.randrange(0, 42)
					ry = random.randrange(0, 42)
				tools_pos.append((rx, ry, i))
				self.nodes[rx][ry].tool = i
		return tools_pos

	#Set fabric positions, returns the positions list
	def set_fabrics(self):
		fabrics_pos = []

		for i in range(f):
			rx = random.randrange(0, 42)
			ry = random.randrange(0, 42)
			while self.nodes[rx][ry].tool != -1 or self.nodes[rx][ry].fabric != -1:
				rx = random.randrange(0, 42)
				ry = random.randrange(0, 42)
			fabric = Fabric(rx, ry)
			fabrics_pos.append(fabric)
			self.nodes[rx][ry].fabric = i
		return fabrics_pos

	#Reset environment for new experiments
	def reset(self, tools, fabrics):
		for row in self.nodes:
			for node in row:
				node.acc = 0
				node.father = None
				node.fixed = False
				node.set_color()
		
		for tool in tools:
			self.nodes[tool[0]][tool[1]].tool = tool[2]

		for fabric in fabrics:
			fabric.status = False
			
	def reprint(self):
		for row in self.nodes:
			for node in row:
				node.set_color()
	
	def print_graph(self):
		for row in self.nodes:
			print(''.join(str(node.cost) for node in row))
	
if __name__ == '__main__':
	pygame.init()

	parser = argparse.ArgumentParser(description='Search robot')
	parser.add_argument('-res', '--resolution', dest='res', type=int, action='store', nargs=1, help="Resolution: screen resolution")
	parser.add_argument('-pos', '--position', dest='pos', action='store', nargs='+', help="Starting position = i j or random")
	parser.add_argument('-r', dest='r', action='store', type=int, nargs=1, help='radius')
	parser.add_argument('-o', dest='o', action='store', nargs=1, help='Ordem: ordenado/nordenado')
	parser.add_argument('-qtf', dest='qtf', action='store', nargs=1, help='Quantidade de ferramentas: enough/all')

	args = parser.parse_args()

	res = args.res[0]
	pos = args.pos
	#ordem = args.o[0]
	#radius = args.r[0]
	#qtf = args.qtf[0]

	start_robot = []
	if pos[0] == 'random':
		n = int(pos[1])
		for i in range(n):
			rx = random.randrange(0, 42)
			ry = random.randrange(0, 42)
			while (rx, ry) in start_robot:
				rx = random.randrange(0, 42)
				ry = random.randrange(0, 42)
			start_robot.append((rx, ry))
	else:
		n = 1.0
		sx = int(args.pos[0])
		sy = int(args.pos[1])
		start_robot.append((sx, sy))
	
	screen = pygame.display.set_mode((res, res), HWSURFACE | DOUBLEBUF)
	sc_w = 5

	#Initialize environment
	env = Graph()
	env.build_graph('Robo_ambiente.txt')
	tools = env.set_tools()
	fabrics = env.set_fabrics()
	env.reprint()
	pygame.image.save(screen, 'InitialConfiguration.png')

	out = open('out.dat', 'w')
	for radius in [4,8]:
		for ordem in ['ordenado', 'nordenado']:
			for qtf in ['all', 'enough']:
				costs = []
				for pos in start_robot:
					env.reset(tools, fabrics)
					sx = pos[0]
					sy = pos[1]

					#Initialize robot
					robot = Robot(sx, sy, radius, env, fabrics)

					if ordem == 'ordenado':
						while not robot.has_all_itens():
							robot.move()
							env.reprint()
							for event in pygame.event.get():
								if event.type == QUIT:
									sys.exit()

						while not robot.fixed_all():
							fab_id = robot.can_fix()
							robot.fix_fabric(fab_id)
							env.reprint()
						costs.append(robot.cost)
					else:
						while not robot.fixed_all():
							robot.move()
						costs.append(robot.cost)
				
				env.reprint()
				pygame.display.flip()
				
				u = mean(costs)
				sd = std(costs)

				print(ordem+'_'+qtf+'_'+str(radius)+'_''Total cost: %.4lf \t u: %.4lf' % (u, sd))
				out.write(ordem+'_'+qtf+'_'+str(radius)+'_Total cost: %.4lf \t u: %.4lf\n' % (u, sd))
	out.close()
